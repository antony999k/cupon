(function() {
    var app = angular.module("moduleMain", ['ngRoute']);

    //Esconde la barra del menu cuando estan activas las rutas del login
    app.run(function($rootScope,$location) {
        $rootScope.$on("$locationChangeStart", function(event, next, current) {
            $rootScope.hideMenuBar = true;
            if($location.path() == "/foreingCard" || $location.path() == "/cancunCard" || $location.path() == "/login" || $location.path() == "/pay" || $location.path() == "/register"){
              $rootScope.hideMenuBar = false;
            }
        });
    });

//Obtiene los datos de los arrays de obejtos y los muestra en las vistas
    app.controller('controladorMain', function() {
        this.cupons = cupons;
        this.places = places;
        this.categories = categories;
        this.brands = brands;
    });

//Hace que aparezca y desaparesca el menu y el filtro
    app.controller('showMenu', ['$scope', function($scope) {
        this.open = false;
        this.open2 = false;
        this.changeClass = function() {
            this.open = !this.open;
            if(this.open2 == true && this.open == true){
              this.open2 = false;
            }
        };
        this.changeClass2 = function() {
            this.open2 = !this.open2;
            if(this.open2 == true && this.open == true){
              this.open = false;
            }
        };

        this.changeClass3 = function() {
            this.open = !this.open;
        };
    }]);


//Jsons
    var cupons = [{
            desc: 'Hyatt Cancún',
            discount: 10,
            img: '/img/cuponOneImg',
            cat: 'Hotel',
            place: 'Isla Dorada',
            brand: 'Ask'
        },
        {
            desc: 'Cupon name two',
            discount: 20,
            img: '/img/cuponTwoImg',
            cat: 'Food',
            place: 'Isla Dorada',
            brand: 'Ask'
        },
        {
            desc: 'Cupon name three',
            discount: 15,
            img: '/img/cuponThreeImg',
            cat: 'Nigth Life',
            place: 'Isla Dorada',
            brand: 'Ask'
        },
        {
            desc: 'Cupon name four',
            discount: 33,
            img: '/img/cuponFourImg',
            cat: 'Food',
            place: 'Isla Dorada',
            brand: 'Google'
        },
        {
            desc: 'Cupon name Five',
            discount: 55,
            img: '/img/cuponFiveImg',
            cat: 'Lodging',
            place: 'Isla Dorada',
            brand: 'Bola loca'
        },
        {
            desc: 'Cupon name Six',
            discount: 12,
            img: '/img/cuponFiveImg',
            cat: 'Food',
            place: 'Isla Dorada',
            brand: 'Marriot'
        },
        {
            desc: 'Cupon name Seven',
            discount: 13,
            img: '/img/cuponSevenImg',
            cat: 'Nigth Life',
            place: 'Isla Dorada',
            brand: 'Apple'
        }
    ]

    var places = [{
            place: 'Playa del carmen'
        },
        {
            place: 'Isla Dorada'
        }
    ]

    var categories = [{
            cat: 'Hotel'
        },
        {
            cat: 'Nigth Life'
        },
        {
            cat: 'Food'
        },
        {
            cat: 'Lodging'
        }
    ]

    var brands = [{
            brand: 'Ask'
        },
        {
            brand: 'Marriot'
        },
        {
            brand: 'Hilton'
        },
        {
            brand: 'Google'
        },
        {
            brand: 'Apple'
        },
        {
            brand: 'Yahoo'
        },
        {
            brand: 'Bola Loca'
        }
    ]


    var cards = [{
            card: 'KH32JBF3283209',
            id: 324245234
        },
        {
            card: 'LKNS7482FN0EFI',
            id: 234232543
        }
    ]







})();
