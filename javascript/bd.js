(function() {
    var app = angular.module("moduleMain", ['ngRoute']);

    app.controller('controladorMain', function() {
        this.cupons = cupons;
    });


    var cupons = [{
            desc: 'Hyatt Cancún',
            discount: 10,
            img: '/img/cuponOneImg'
        },
        {
            desc: 'Cupon name two',
            discount: 20,
            img: '/img/cuponTwoImg'
        },
        {
            desc: 'Cupon name three',
            discount: 15,
            img: '/img/cuponThreeImg'
        },
        {
            desc: 'Cupon name four',
            discount: 33,
            img: '/img/cuponFourImg'
        },
        {
            desc: 'Cupon name Five',
            discount: 55,
            img: '/img/cuponFiveImg'
        }
    ]

})();
