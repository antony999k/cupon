/*Rutas de la aplicaión*/

angular.module("moduleMain").config(["$routeProvider","$locationProvider",function($routeProvider,$locationProvider){
    $locationProvider.hashPrefix('');
    $routeProvider.when("/", {
        templateUrl: "templates/pages/main/index.html",
        controller:"controladorMain"
    }).when("/cuponList",{
        templateUrl: "templates/pages/cuponList/index.html",
        controller:"controladorMain"
    }).when("/foreingCard",{
        templateUrl: "templates/pages/foreingCard/index.html",
        controller:"controladorMain"
    }).when("/cancunCard",{
        templateUrl: "templates/pages/cancunCard/index.html",
        controller:"controladorMain"
    }).when("/login",{
        templateUrl: "templates/pages/login/index.html",
        controller:"controladorMain"
    }).when("/pay",{
        templateUrl: "templates/pages/pay/index.html",
        controller:"controladorMain"
    }).when("/register",{
        templateUrl: "templates/pages/register/index.html",
        controller:"controladorMain"
    }).when("/profile",{
        templateUrl: "templates/pages/profile/index.html",
        controller:"controladorMain"
    }).when("/wallet",{
        templateUrl: "templates/pages/wallet/index.html",
        controller:"controladorMain"
    }).when("/categories",{
        templateUrl: "templates/pages/categories/index.html",
        controller:"controladorMain"
    }).when("/places",{
        templateUrl: "templates/pages/places/index.html",
        controller:"controladorMain"
    }).when("/search",{
        templateUrl: "templates/pages/search/index.html",
        controller:"controladorMain"
    }).when("/brands",{
        templateUrl: "templates/pages/brands/index.html",
        controller:"controladorMain"
    }).when("/reedeemCupon",{
        templateUrl: "templates/pages/reedeemCupon/index.html",
        controller:"controladorMain"
    }).when("/near",{
        templateUrl: "templates/pages/near/index.html",
        controller:"controladorMain"
    }).when("/brandProfile",{
        templateUrl: "templates/pages/brandProfile/index.html",
        controller:"controladorMain"
    }).when("/cuponDetails",{
        templateUrl: "templates/pages/cuponDetails/index.html",
        controller:"controladorMain"
    }).when("/lastMinute",{
        templateUrl: "templates/pages/lastMinute/index.html",
        controller:"controladorMain"
    })/*.otherwise("/main")*/;
}]);
